<?php

declare(strict_types=1);

namespace App\Controller\DataBase;

use App\Controller\AppController;
use MongoDB\Client;

/**
 * Nosql Controller
 *
 * @method \App\Model\Entity\Nosql[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NosqlController extends AppController
{
    private function connect()
    {
        $client = new Client("mongodb://mongo:mongo@projet-noSql-mongodb:27017");
        return $client->projetnosql->Posts;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->check('UserId'))
            return $this->redirect([
                'controller' => 'Users',
                'prefix' => 'DataBase',
                'action' => 'index'
            ]);

        $collection = $this->connect();

        $cursor = $collection->find();
        $username = $session->read('UserLogin');

        $this->set(compact('cursor', 'username'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if (!$session->check('UserId'))
            return $this->redirect([
                'controller' => 'Users',
                'prefix' => 'DataBase',
                'action' => 'index'
            ]);

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $fileobject = $data['submittedfile'];
            $fileName = $fileobject->getClientFilename();
            if ($data['Titre'] == '' || $fileName  == '') {
                $this->Flash->error(__('Titre et Photo obligatoire pour un post.'));
                return;
            }
            $insert = [
                'titre' => $data['Titre'],
                'created' => date("d-m-Y"),
                'by' => $session->read('UserLogin')
            ];

            if ($data['Description'] !== '')
                $insert['description'] = $data['Description'];

            $collection = $this->connect();
            $result = $collection->insertOne($insert);
            $insertedId = $result->getInsertedId();

            $uploadPath = WWW_ROOT . 'resources' . DS;
            $fileName = explode('.', $fileName);
            $fileName = $insertedId . '.' . $fileName[count($fileName) - 1];
            $destination = $uploadPath . $fileName;
            $fileobject->moveTo($destination);

            $collection->updateOne(
                ['_id' => $insertedId],
                ['$set' => ['image' => $fileName]]
            );

            return $this->redirect([
                'controller' => 'Nosql',
                'prefix' => 'DataBase',
                'action' => 'index'
            ]);
        }
    }

    /**
     * Like method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function like($idpost)
    {
        $session = $this->request->getSession();
        if (!$session->check('UserId'))
            return $this->redirect([
                'controller' => 'Users',
                'prefix' => 'DataBase',
                'action' => 'index'
            ]);

        if ($this->request->is('get')) {
            $username = $session->read('UserLogin');

            $collection = $this->connect();

            $idpost = new \MongoDB\BSON\ObjectId("$idpost");

            $cursor = $collection->findOne(['_id' => $idpost]);

            if (!isset($cursor['likes']) || !in_array($username, $cursor['likes']->jsonSerialize())) {
                $collection->updateOne(
                    ['_id' => $idpost],
                    ['$push' => ['likes' => $username]]
                );
            } else {
                $collection->updateOne(
                    ['_id' => $idpost],
                    ['$pull' => ['likes' => $username]]
                );
            }
        }

        return $this->redirect([
            'controller' => 'Nosql',
            'prefix' => 'DataBase',
            'action' => 'index'
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Controller\DataBase;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $user = $this->Users->newEmptyEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $user = $this->Users
                ->find()
                ->where(['login' => $data['login']])
                ->first();

            $hasher = new DefaultPasswordHasher();
            if ($user != null && $hasher->check($data['password'], $user->password)) {
                $this->Flash->success(__('Connecté.'));

                $session = $this->request->getSession();
                $session->write('UserId', $user->id);
                $session->write('UserLogin', $user->login);

                return $this->redirect([
                    'controller' => 'Nosql',
                    'prefix' => 'DataBase',
                    'action' => 'index'
                ]);
            }

            $this->Flash->error(__('Incorect Login or Password.'));
        }

        $this->set(compact('user'));
    }

    /**
     * Get method
     *
     * @return User|null
     */
    public function get(string $login, string $passwd)
    {
        $user = $this->Users
            ->find()
            ->where(['login', $login])
            ->first();

        $hasher = new DefaultPasswordHasher();
        if ($hasher->check($passwd, $user->password)) {
            return $user;
        } else {
            return null;
        }
    }

    public function disconnect()
    {
        $session = $this->request->getSession()->destroy();

        return $this->redirect([
            'controller' => 'Users',
            'prefix' => 'DataBase',
            'action' => 'index'
        ]);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $user->login = $data['login'];
            $hasher = new DefaultPasswordHasher();
            $user->password = $hasher->hash($data['password']);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                $session = $this->request->getSession();
                $session->write('UserId', $user->id);

                return $this->redirect([
                    'controller' => 'Nosql',
                    'prefix' => 'DataBase',
                    'action' => 'index'
                ]);
            }

            $this->Flash->error(__('The User could not be saved. Please, try again.'));
        }

        $this->set(compact('user'));
    }
}

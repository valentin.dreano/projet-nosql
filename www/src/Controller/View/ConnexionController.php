<?php

declare(strict_types=1);

namespace App\Controller\View;

use App\Controller\AppController;
use App\Controller\DataBase\UsersController as DataBaseUsersController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConnexionController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $user = $this->Users->newEmptyEntity();
        $this->set(compact($user));
    }
}

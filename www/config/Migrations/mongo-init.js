var db = connect("mongodb://mongo:mongo@127.0.0.1:27017");

db = db.getSiblingDB('projetnosql'); // we can not use "use" statement here to switch db

db.createCollection('Posts');

db.Posts.insert({
    titre: "Defaite de la FIIPALE",
    description: "Malheureusement la FIIPALE a perdu et n'a même pas passé le 1er tour. Nous avons tout de meme fini 3eme.",
    image: "image1.png",
    created: "10-12-2021",
    by: "OxOrio",
    likes: [
        "Mana",
        "Bebel"
    ]
});

db.Posts.insert({
    titre: "Moi après une fin de séance à l'escalade",
    image: "image2.png",
    created: "15-10-2021",
    by: "OxOrio",
    likes: [
        "AnneSo",
        "Skipper",
        "Bebel"
    ]
});

db.Posts.insert({
    titre: "Tryhard Pokémon",
    description: "Go tryhard ce jeu avec Antoine Man gogogo",
    image: "image3.png",
    created: "13-12-2021",
    by: "OxOrio"
});
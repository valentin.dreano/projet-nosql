drop table if exists users cascade;
create table if not exists users (
    id serial constraint users_pk primary key,
    login varchar(128),
    password varchar(256)
);
create unique index if not exists users_login_uindex on users (login);
insert into public.users (login, password)
values (
        'AnneSo',
        '$2y$10$UG6B.JYt2YSP68hkOQwon.nVTgiC2j6A2tJEPldpg1w8R6sYyf7QC'
    ),
    (
        'Mana',
        '$2y$10$UG6B.JYt2YSP68hkOQwon.nVTgiC2j6A2tJEPldpg1w8R6sYyf7QC'
    ),
    (
        'Skipper',
        '$2y$10$UG6B.JYt2YSP68hkOQwon.nVTgiC2j6A2tJEPldpg1w8R6sYyf7QC'
    ),
    (
        'OxOrio',
        '$2y$10$UG6B.JYt2YSP68hkOQwon.nVTgiC2j6A2tJEPldpg1w8R6sYyf7QC'
    ),
    (
        'Bebel',
        '$2y$10$UG6B.JYt2YSP68hkOQwon.nVTgiC2j6A2tJEPldpg1w8R6sYyf7QC'
    );
<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <?= $this->Html->link(
                __('Sign up'),
                ['action' => 'add'],
                [
                    'class' => 'btn btn-primary',
                    'style' => 'background-color: #d33c43;
                    border: 0.1rem solid #d33c43;
                    border-radius: .4rem;
                    color: #fff;
                    cursor: pointer;
                    display: inline-block;
                    font-size: 1.1rem;
                    font-weight: 700;
                    height: 3.8rem;
                    letter-spacing: .1rem;
                    line-height: 3.8rem;
                    padding: 0 3.0rem;
                    text-align: center;
                    text-decoration: none;
                    text-transform: uppercase;
                    white-space: nowrap;'
                ]
            ) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
            <?= $this->Form->create($user) ?>
            <fieldset>
                <legend><?= __('Connexion') ?></legend>
                <?php
                echo $this->Form->control('login');
                echo $this->Form->control('password');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Sign in')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
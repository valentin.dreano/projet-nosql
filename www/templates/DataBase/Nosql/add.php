<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <?= $this->Html->link(
                __('Liste des posts'),
                [
                    'controller' => 'Nosql',
                    'prefix' => 'DataBase',
                    'action' => 'index'
                ],
                [
                    'class' => 'btn btn-primary',
                    'style' => 'background-color: #d33c43;
                    border: 0.1rem solid #d33c43;
                    border-radius: .4rem;
                    color: #fff;
                    cursor: pointer;
                    display: inline-block;
                    font-size: 1.1rem;
                    font-weight: 700;
                    height: 3.8rem;
                    letter-spacing: .1rem;
                    line-height: 3.8rem;
                    padding: 0 3.0rem;
                    text-align: center;
                    text-decoration: none;
                    text-transform: uppercase;
                    white-space: nowrap;'
                ]
            ) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
            <?= $this->Form->create(NULL, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('New element') ?></legend>
                <?= $this->Form->control('Titre'); ?>
                <?= $this->Form->control('Description'); ?>
                <?php
                echo $this->Form->file('submittedfile');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
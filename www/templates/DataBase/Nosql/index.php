<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="container-sm">
    <?= $this->Html->link(
        __('Sign out'),
        [
            'controller' => 'Users',
            'prefix' => 'DataBase',
            'action' => 'disconnect'
        ],
        [
            'class' => 'btn btn-primary',
            'style' => 'background-color: #d33c43;
            border: 0.1rem solid #d33c43;
            border-radius: .4rem;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-size: 1.1rem;
            font-weight: 700;
            height: 3.8rem;
            letter-spacing: .1rem;
            line-height: 3.8rem;
            padding: 0 3.0rem;
            text-align: center;
            text-decoration: none;
            text-transform: uppercase;
            white-space: nowrap;'
        ]
    ) ?>
    <?= $this->Html->link(
        __('Nouveau post'),
        [
            'controller' => 'Nosql',
            'prefix' => 'DataBase',
            'action' => 'add'
        ],
        [
            'class' => 'btn btn-primary',
            'style' => 'background-color: #d33c43;
            border: 0.1rem solid #d33c43;
            border-radius: .4rem;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-size: 1.1rem;
            font-weight: 700;
            height: 3.8rem;
            letter-spacing: .1rem;
            line-height: 3.8rem;
            padding: 0 3.0rem;
            text-align: center;
            text-decoration: none;
            text-transform: uppercase;
            white-space: nowrap;'
        ]
    ) ?>
</div>
<div class="container-fluid">
    <div class="row row-cols-1 row-cols-md-4 g-4">
        <?php
        $cursor->setTypeMap(array(
            'array' => 'array',
            'document' => 'array',
            'root' => 'array'
        ));

        foreach ($cursor as $document) :
        ?>
            <div class="col">
                <div class="card h-100">
                    <img src="<?= 'resources' . DS . $document['image'] ?>" class="card-img-top" alt="..." style="max-width: 254px">
                    <div class="card-body">
                        <h5 class="card-title"><?= h($document['titre']) ?></h5>
                        <p class="card-text"><?= isset($document['description']) ? h($document['description']) : '' ?></p>
                    </div>
                    <div class="card-footer">
                        <?php
                        $color = '';
                        $likes = '0 Likes';
                        if (isset($document['likes'])) {
                            $nbrLike = count($document['likes']);
                            if ($nbrLike > 0) {
                                if (in_array($username, $document['likes'])) {
                                    $color = 'color: red;';
                                }
                            }

                            $likes = $nbrLike . ' Likes';
                        }
                        ?>
                        <a href="/like/<?= h($document['_id']) ?>">
                            <p style="padding-left: 3px;">
                                <i class="fas fa-heart" style="padding-right: 3px;<?= $color ?>"></i>
                                <?= $likes ?>
                            </p>
                        </a>
                        <br>
                        <small class="text-muted">Publié le <?= h($document['created']) ?> by <?= $document['by'] ?></small>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
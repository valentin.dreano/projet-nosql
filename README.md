# Projet-nosql

Valentin DREANO

## Commande du projet 

La première fois pour installer le projet il faut faire la commande suivante :
```bash
make install
```

Ensuite, pour le démarrer les fois suivante :
```bash
make start
```
Et pour l'arréter :
```bash
make stop
```
Si vous voulez supprimer tous les fichiers et repartir d'une instance de base :
```bash
make uninstall
```

## Technologie utlisé
- NGINX : utilisé comme serveur web
- PHP : language de programation utlisé
- Postgres : Base de donnée relationelle
- MongoDB : Base de données NoSql

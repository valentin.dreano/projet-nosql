start:
	docker-compose up --build -d
	sleep 3

stop:
	docker-compose down -v

restart: stop start

install: uninstall start 

uninstall: stop
	@sudo rm -rf docker/postgres/data 2 >> /dev/null
	@sudo rm -rf docker/mongo/share/* 2 >> /dev/null
	@sudo rm -rf docker/mongo/state/* 2 >> /dev/null
